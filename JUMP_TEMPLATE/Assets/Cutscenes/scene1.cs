﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scene1 : MonoBehaviour
{
    public Sprite sprite1;
    public Sprite sprite2;
    public Sprite sprite3;
    public Sprite sprite4;
    public Sprite sprite5;
    private SpriteRenderer spriteRenderer;
    public bool moving = false;
    public bool cutscene1;
    public bool cutscene2;
    public bool cutscene3;
    public bool cutscene4;
    public bool cutscene5;
    public bool cutscene6;
    float shutterposition;
    [SerializeField] private AudioSource shutternoise;
    [SerializeField] private AudioClip ACshutternoise;
    // Start is called before the first frame update
    void Start()
    {
        shutternoise = GetComponent<AudioSource>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        if (spriteRenderer.sprite == null) // if sprite renderer is null get the first image
        {
            spriteRenderer.sprite = sprite1;
        }
    }

    // Update is called once per frame
    void Update()
    {
   
            shutterposition = GameObject.Find("shutter").transform.position.y;
            if (Input.GetMouseButtonUp(0))
            {
                moving = true;
            }
            if (moving == true)
            {
                GameObject.Find("shutter").transform.position = new Vector3(0, shutterposition - 1, 0);
            }
            if (GameObject.Find("shutter").transform.position == new Vector3(0, 0, 0))
            {
            if (!shutternoise.isPlaying)
            {
                shutternoise.PlayOneShot(ACshutternoise);
            }
            GameObject.Find("shutter").transform.position = new Vector3(0, 10, 0);
                moving = false;
                Changesprite();
            }
        
    }
    void Changesprite()
    {
        if (cutscene1 == true)
        {
            if (spriteRenderer.sprite == sprite1)
            {
                spriteRenderer.sprite = sprite2;
            }
            else if (spriteRenderer.sprite == sprite2)
            {
                spriteRenderer.sprite = sprite3;
            }
            else if (spriteRenderer.sprite == sprite3)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                cutscene1 = false;
                cutscene2 = true;
            }
        }
        if (cutscene2 == true)
        {
            if (spriteRenderer.sprite == sprite1)
            {
                spriteRenderer.sprite = sprite2;
            }
            else if (spriteRenderer.sprite == sprite2)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                cutscene2 = false;
                cutscene3 = true;
            }
        }
        if (cutscene3 == true)
        {
            if (spriteRenderer.sprite == sprite1)
            {
                spriteRenderer.sprite = sprite2;
            }
            else if (spriteRenderer.sprite == sprite2)
            {
                spriteRenderer.sprite = sprite3;
            }
            else if (spriteRenderer.sprite == sprite3)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                cutscene3 = false;
                cutscene4 = true;
            }
        }
        if (cutscene4 == true)
        {
            if (spriteRenderer.sprite == sprite1)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                cutscene4 = false;
                cutscene5 = true;
            }
        }
        if (cutscene5 == true)
        {
            if (spriteRenderer.sprite == sprite1)
            {
                spriteRenderer.sprite = sprite2;
            }
            else if (spriteRenderer.sprite == sprite2)
            {
                spriteRenderer.sprite = sprite3;
            }
            else if (spriteRenderer.sprite == sprite3)
            {
                spriteRenderer.sprite = sprite4;
            }
            else if (spriteRenderer.sprite == sprite4)
            {
                spriteRenderer.sprite = sprite5;
            }
            else if (spriteRenderer.sprite == sprite5)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                cutscene5 = false;
                cutscene6 = true;
            }
        }
        if (cutscene6 == true)
        {
            if (spriteRenderer.sprite == sprite1)
            {
                spriteRenderer.sprite = sprite2;
            }
            else if (spriteRenderer.sprite == sprite2)
            {
                spriteRenderer.sprite = sprite3;
            }
            else if (spriteRenderer.sprite == sprite3)
            {
                spriteRenderer.sprite = sprite4;
            }
            else if (spriteRenderer.sprite == sprite4)
            {
                SceneManager.LoadScene("Mainmenu/Main Menu1");
                cutscene6 = false;
            }
        }
    }
}
