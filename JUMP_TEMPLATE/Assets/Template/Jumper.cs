﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.SceneManagement;
// Player functionality

public class Jumper : MonoBehaviour
{
    
    bool canfly = false;
    bool fromfox = false;
    float wasfox = 0f;
    float flytime = 1f;
    public float flydecay;
    public float jumpStrength;
    public float negJumpLength;
    public int Type = 0;
    public float bearSpeed = 0.003f;
    public float fastBearSpeed;
    public float armadilloSpeed = 0.002f;
    public float foxSpeed = 0.05f;
    public float foxSpeedCap = 1f;
    public float birdx;
    public float birdy;
    public float dillograv;
    public Sprite BearSprite;
    public Sprite BirdSprite;
    public Sprite FoxSprite;
    public Sprite ArmadilloSprite;  
    public Sprite BirdSpriteMoving;
    public Sprite FoxSpriteMoving;
    public Sprite ArmadilloSpriteMoving;
    public RuntimeAnimatorController BirdAnim;
    public RuntimeAnimatorController ArmadilloAnim;
    public RuntimeAnimatorController FoxAnim;
    public PhysicsMaterial2D stickyMat;
    public PhysicsMaterial2D slippyMat;
    public int nextLevelNo;
    int birdlook = 0;
    bool isGrounded = false;
    public Transform isGroundedChecker;
    public float checkGroundRadius;
    public LayerMask groundLayer;
    bool isNotBlocked = false;
    public Transform isBlockedChecker; 
    bool isNotBlockedAlt = false;
    public Transform isBlockedCheckerAlt;
    public float checkPathRadius;
    [SerializeField] private AudioSource foxjump;
    [SerializeField] private AudioSource spikekill;
    [SerializeField] private AudioSource birdflap;
    [SerializeField] private AudioSource transformation;
    [SerializeField] private AudioClip ACbirdflap;
    [SerializeField] private AudioClip ACtransformation;
    [SerializeField] private AudioClip ACspikekill;
    // The Update function is called every render frame by Unity to handle object processing (can be public/private/protected/etc.)
    private int collisionCount = 0;
    public int transformlimit;
    GameObject centre;
    public void Start()
    {
        foxjump = GetComponent<AudioSource>();
        spikekill = GetComponent<AudioSource>();
        birdflap = GetComponent<AudioSource>();
    }
    void CheckIfGrounded()
    {
        Collider2D collider = Physics2D.OverlapCircle(isGroundedChecker.position, checkGroundRadius, groundLayer);
        if (collider != null)
        {
            isGrounded = true;
        }
        else
        {
            isGrounded = false;
        }
    }
    void CheckIfSlideIn()
    {
        Collider2D collider = Physics2D.OverlapCircle(isBlockedChecker.position, checkPathRadius, groundLayer);
        if (collider != null)
        {
            isNotBlocked = false;
        }
        else
        {
            isNotBlocked = true;
        }
    }
    void CheckIfSlideInAlt()
    {
        Collider2D collider = Physics2D.OverlapCircle(isBlockedCheckerAlt.position, checkPathRadius, groundLayer);
        if (collider != null)
        {
            isNotBlockedAlt = false;
        }
        else
        {
            isNotBlockedAlt = true;
        }
    }

    void birdswitcher()
    {
        if (birdlook == 0)
        {
            GetComponent<SpriteRenderer>().sprite = BirdSprite;
        }
        else if (birdlook == 1)
        {
            GetComponent<SpriteRenderer>().sprite = BirdSpriteMoving;
        }
        else
        {
            birdlook = 0;
        }
    }
    public bool IsNotColliding
    {
        get { return collisionCount == 0; }
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        collisionCount++;        
    }

    void OnCollisionExit2D(Collision2D col)
    {
        collisionCount--;        
    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "spikes") {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            if (!spikekill.isPlaying)
            {
                spikekill.PlayOneShot(ACspikekill);
            }
        }       

       
        if (other.gameObject.tag == "nextlevel")
        {
            SceneManager.LoadScene(nextLevelNo);                       
        }          
    }
    private void OnTriggerExit2D(Collider2D collision)
    {        
               
    }
    private void Update()
    {
        CheckIfGrounded();
        CheckIfSlideIn();
        CheckIfSlideInAlt();
        
        if (Input.GetKeyUp("1")) 
        {
            this.Type = 1;
            if (!transformation.isPlaying)
            {
                transformation.PlayOneShot(ACtransformation);
            }
        }
        if (Input.GetKeyUp("2")) 
        {
            this.Type = 0;
            if (!transformation.isPlaying)
            {
                transformation.PlayOneShot(ACtransformation);
            }
        }
        if (Input.GetKeyUp("3") && transformlimit >= 1)
        {
            this.Type = 2;
            if (!transformation.isPlaying)
            {
                transformation.PlayOneShot(ACtransformation);
            }
        }
        if (Input.GetKeyUp("4") && transformlimit == 2)
        {
            this.Type = 3;
            if (!transformation.isPlaying)
            {
                transformation.PlayOneShot(ACtransformation);
            }
        } 
        if (Input.GetKeyUp("r"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        if (wasfox > 0)
        {
            wasfox -= Time.deltaTime;
            fromfox = true;
        }
        else
        {
            fromfox = false;
        }
        if (flytime > 0)
        {
            flytime -= Time.deltaTime;
            canfly = false;
        }
        if (flytime < 0)
        {
            canfly = true;
        }
        if (this.Type == 1)
        {
            this.tag = "bird";
            birdswitcher();
            GetComponent<Rigidbody2D>().gravityScale = 3;
            GetComponent<Animator>().runtimeAnimatorController = BirdAnim as RuntimeAnimatorController;                                              
            GetComponent<CapsuleCollider2D>().sharedMaterial = slippyMat;
            GetComponent<CapsuleCollider2D>().size = new Vector2(2.31481f, 0.9233435f);
            GetComponent<CapsuleCollider2D>().offset = new Vector2(0.1211662f, -0.1406579f);
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            this.transform.localScale = new Vector3(0.3f, 0.5f, 1f);
            this.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
            
            if (Input.GetKeyUp("right") && canfly)
            {
                birdlook += 1;
                GetComponent<Rigidbody2D>().velocity = new Vector2(birdx, birdy);
                flytime = flydecay;
                if (!birdflap.isPlaying)
                {
                    birdflap.PlayOneShot(ACbirdflap);
                }
                GetComponent<SpriteRenderer>().flipX = false;
            }
            if (Input.GetKeyUp("left") && canfly)
            {
                birdlook += 1;
                GetComponent<Rigidbody2D>().velocity = new Vector2(-birdx, birdy);
                flytime = flydecay;
                if (!birdflap.isPlaying)
                {
                    birdflap.PlayOneShot(ACbirdflap);
                }
                GetComponent<SpriteRenderer>().flipX = true;
            }                        
        }
    }
    private void FixedUpdate()        
    {
        centre = transform.Find("Centre").gameObject;
        if (this.Type == 0)
        {
            this.tag = "bear";
            GetComponent<Rigidbody2D>().gravityScale = 3;
            GetComponent<Animator>().enabled = false;
            GetComponent<SpriteRenderer>().sprite = BearSprite;
            GetComponent<CapsuleCollider2D>().sharedMaterial = slippyMat;
            GetComponent<CapsuleCollider2D>().size = new Vector2(2.0f, 2.0f);
            GetComponent<CapsuleCollider2D>().offset = new Vector2(0.0f, 0.2f);
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            this.transform.localScale = new Vector3(1f, 1f, 1f);
            centre.transform.Find("GroundChecker").transform.localPosition = new Vector3(0.004f, -1f, -0.5541992f);
            centre.transform.Find("BlockChecker").transform.localPosition = new Vector3(1.0f, 0.036f, -1.363308f);
            centre.transform.Find("BlockCheckerAlt").transform.localPosition = new Vector3(-1.0f, 0.036f, -1.464111f);
            float moveHorizontal = Input.GetAxisRaw("Horizontal");            
            Vector2 movement = new Vector2(moveHorizontal, 0);
            Debug.Log(isGrounded);
            if ((isNotBlocked || isNotBlockedAlt) && isGrounded && fromfox == false)
            {
                GetComponent<Rigidbody2D>().velocity = movement*bearSpeed;
               

            }
            else if ((isNotBlocked || isNotBlockedAlt) && isGrounded && fromfox == true)
            {
                GetComponent<Rigidbody2D>().velocity = movement * fastBearSpeed;
            }
        }
        
        if (this.Type == 2)
        {
            this.tag = "fox";
            GetComponent<Rigidbody2D>().gravityScale = 3;
            GetComponent<Animator>().runtimeAnimatorController = FoxAnim as RuntimeAnimatorController;
            
            if (GetComponent<Rigidbody2D>().velocity.magnitude <= 3 || !isGrounded)
            {
                GetComponent<Animator>().enabled = false;
                this.transform.localScale = new Vector3(0.6f, 0.6f, 1f);
                GetComponent<SpriteRenderer>().sprite = FoxSprite;
                GetComponent<CapsuleCollider2D>().size = new Vector2(2.06f, 0.896f);
                GetComponent<CapsuleCollider2D>().offset = new Vector2(0.0f, 0.0f);
                centre.transform.Find("GroundChecker").transform.localPosition = new Vector3(0.022f, -0.75f, -0.5541992f);
                centre.transform.Find("BlockChecker").transform.localPosition = new Vector3(1.0f, -0.00009f, -1.363308f);
                centre.transform.Find("BlockCheckerAlt").transform.localPosition = new Vector3(-1.0f, 0.0017f, -1.464111f);
            }           
            else
            {
                GetComponent<Animator>().enabled = true;
                GetComponent<CapsuleCollider2D>().size = new Vector2(0.3f, 0.1f);
                GetComponent<SpriteRenderer>().sprite = FoxSpriteMoving;
                GetComponent<CapsuleCollider2D>().offset = new Vector2(0.0f, 0.0f);
                this.transform.localScale = new Vector3(4.48f, 4.48f, 1f);
                centre.transform.Find("GroundChecker").transform.localPosition = new Vector3(0.0f, -0.25f, -0.5541992f);
                centre.transform.Find("BlockChecker").transform.localPosition = new Vector3(0.98f, -0.3f, -1.363308f);
                centre.transform.Find("BlockCheckerAlt").transform.localPosition = new Vector3(-1.0f, -0.3f, -1.464111f);
            }
            
            GetComponent<CapsuleCollider2D>().sharedMaterial = stickyMat;
            
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            this.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
            wasfox = 1f;
            
            float moveHorizontal = Input.GetAxisRaw("Horizontal");
            Vector2 movement = new Vector2(moveHorizontal, 0);
            
            if (GetComponent<Rigidbody2D>().velocity.magnitude < foxSpeedCap && isNotBlocked && isNotBlockedAlt) 
            {
                GetComponent<Rigidbody2D>().AddForce(movement * foxSpeed, ForceMode2D.Impulse);
            }
            if (GetComponent<Rigidbody2D>().velocity.magnitude < foxSpeedCap && isGrounded )
            {
                GetComponent<Rigidbody2D>().AddForce(movement * foxSpeed, ForceMode2D.Impulse);
            }
            if (Input.GetKey("right"))
            {
                GetComponent<SpriteRenderer>().flipX = false;
                
            }
            if (Input.GetKey("left"))
            {
                GetComponent<SpriteRenderer>().flipX = true;

            }

            //Jumping
            if (Input.GetKey("up") && isGrounded)
            {
                Vector2 jumpdis = new Vector2(-negJumpLength, jumpStrength);
                GetComponent<Rigidbody2D>().AddForce ( jumpdis, ForceMode2D.Impulse);
                foxjump.Play();
            }
            
        }
        if (this.Type == 3)
        {
            this.tag = "armadillo";
            GetComponent<SpriteRenderer>().sprite = ArmadilloSprite;
            GetComponent<Animator>().runtimeAnimatorController = ArmadilloAnim as RuntimeAnimatorController;
            GetComponent<CapsuleCollider2D>().sharedMaterial = slippyMat;
            GetComponent<CapsuleCollider2D>().size = new Vector2(2.31481f,1.1f);
            GetComponent<CapsuleCollider2D>().offset = new Vector2(0.1211662f, -0.1406579f);
            this.transform.localScale = new Vector3(0.5f, 0.5f, 1f);
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
            GetComponent<Rigidbody2D>().gravityScale = dillograv;
            centre.transform.Find("GroundChecker").transform.localPosition = new Vector3(0.004f, -0.9f, -0.5541992f);
            centre.transform.Find("BlockChecker").transform.localPosition = new Vector3(1.357f, -0.196f, -1.363308f);
            centre.transform.Find("BlockCheckerAlt").transform.localPosition = new Vector3(-1.032f, -0.115f, -1.464111f);
            
            if (GetComponent<Rigidbody2D>().velocity.magnitude > 0 )//|| !isGrounded)
            {
               
                GetComponent<Animator>().enabled = true;
                
                GetComponent<SpriteRenderer>().sprite = ArmadilloSpriteMoving;
                
                
            }

            else
            {
               

                GetComponent<Animator>().enabled = false;

                GetComponent<SpriteRenderer>().sprite = ArmadilloSprite;
                
            }
            this.transform.localRotation = new Quaternion(0.0f, 0.0f, 0.0f, 0.0f);
            
            float moveHorizontal = Input.GetAxisRaw("Horizontal");
            Vector2 movement = new Vector2(moveHorizontal, 0);
            if ((isNotBlocked || isNotBlockedAlt) && isGrounded)
            {
                GetComponent<Rigidbody2D>().velocity = movement * armadilloSpeed;
            }
            

            if (Input.GetKey("right"))
            {
                GetComponent<SpriteRenderer>().flipX = false;

            }
            if (Input.GetKey("left"))
            {
                GetComponent<SpriteRenderer>().flipX = true;

            }
        }
    }
}
