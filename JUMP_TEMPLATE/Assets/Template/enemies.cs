﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.SceneManagement;

public class enemies : MonoBehaviour
{
    int counter;
    public Sprite mainSpr;
    public Sprite explodeySpr;
    public float enemiesSpeed = 0.5f;
    private Transform playerTransform;
    private Rigidbody2D rb;
    bool colliding = false;
    string collidedWith;
    public float deathtime;
    
    [SerializeField] private AudioSource explodybois;
    // Start is called before the first frame update
    void Start()
    {
        explodybois = GetComponent<AudioSource>();
        GetComponent<Animator>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        playerTransform = GameObject.Find("MainPlayer").transform;
        float playerDistance = Mathf.Sqrt((Mathf.Pow(playerTransform.position.x - this.transform.position.x, 2)) + (Mathf.Pow(playerTransform.position.y - this.transform.position.y, 2)));
        
        if (playerDistance < 10)
        {
            GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.None;
            Vector3 moveDir = (playerTransform.position - this.transform.position).normalized;
            Vector2 movement;
            movement.x = moveDir.x * enemiesSpeed * Time.deltaTime;
            movement.y = moveDir.y * enemiesSpeed * Time.deltaTime; 
            GetComponent<Rigidbody2D>().velocity = movement;
        }
        
        
        
       
       
    }
    private void FixedUpdate()
    {
        if (colliding == true)
        {
            counter++;
        } 
        if (collidedWith == "armadillo" && counter == deathtime)
        {
            Destroy(gameObject);
            explodybois.Play();
        }
        else if (collidedWith == "bear" && counter == deathtime)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            explodybois.Play();
        }
        else if (collidedWith == "fox" && counter == deathtime)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            explodybois.Play();
        }
        else if (collidedWith == "bird" && counter == deathtime)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            explodybois.Play();
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "MainPlayer")
        {
            GetComponent<Animator>().enabled = true;
            colliding = true;
            collidedWith = collision.gameObject.tag;
            GetComponent<CapsuleCollider2D>().enabled = false;
        } 
    }
    public void OnBecameInvisible()
    {
        Destroy(gameObject);
    }
}
