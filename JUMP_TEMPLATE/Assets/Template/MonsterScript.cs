﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.SceneManagement;

public class MonsterScript : MonoBehaviour
{
    public float wallSpeed;
    [SerializeField] private AudioSource monsterkill;
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.name == "MainPlayer")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            monsterkill.Play();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        monsterkill = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        this.transform.position = new Vector2(this.transform.position.x + wallSpeed * Time.deltaTime, Camera.main.transform.position.y);
    }
    private void FixedUpdate()
    {
        //this.transform.position = new Vector2(this.transform.position.x + wallSpeed, this.transform.position.y);
    }
    
}
