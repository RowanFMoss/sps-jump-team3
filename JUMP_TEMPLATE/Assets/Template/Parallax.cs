﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    
    
    [SerializeField] private Vector2 parallaxeffectmultiplier;

    private Transform cameraTransform;
    private Vector3 lastcameraPosition;
    private float textureUnitSizeX;
    private float textureUnitSizeY;
    private void Start()
    {
        cameraTransform = Camera.main.transform; // get main camera
        lastcameraPosition = new Vector3(cameraTransform.position.x, -2f, 0); // setting to the camera position

        Sprite sprite = GetComponent<SpriteRenderer>().sprite;
        Texture2D texture = sprite.texture;
        textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
        textureUnitSizeY = texture.height / sprite.pixelsPerUnit;
    }
    private void LateUpdate()
    {
        Vector3 deltaMovement = cameraTransform.position - lastcameraPosition; // how much the camera has moved
        transform.position += new Vector3(deltaMovement.x * parallaxeffectmultiplier.x, deltaMovement.y * parallaxeffectmultiplier.y); // moves the background
        lastcameraPosition = cameraTransform.position; // reset our camera position to current position
    }
}
