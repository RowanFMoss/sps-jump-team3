﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class Destruction : MonoBehaviour
{
    public bool broken = false;
    Vector2 initialpos;
    Vector2 launchpath;
    Rigidbody2D dest_rigidbody;
    [SerializeField] private AudioSource bearhitwall;
    [SerializeField] private AudioClip ACbearhitwall;

    // Start is called before the first frame update
    void Start()
    {   
        dest_rigidbody = GetComponent<Rigidbody2D>();
        initialpos = this.transform.position;
        dest_rigidbody.constraints = RigidbodyConstraints2D.FreezePosition;
        bearhitwall = GetComponent<AudioSource>();
    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if (col.gameObject.tag == "bear") 
        { 
            broken = true;
            dest_rigidbody.constraints = RigidbodyConstraints2D.None;
            GetComponent<Rigidbody2D>().gravityScale = 1;
            launchpath = (col.gameObject.transform.position - this.transform.position).normalized;
            GetComponent<Rigidbody2D>().AddForce(-launchpath * 3, ForceMode2D.Impulse);
            GetComponent<BoxCollider2D>().enabled = false;
            if (!bearhitwall.isPlaying)
            {
                bearhitwall.PlayOneShot(ACbearhitwall);
            }
        }
        
    }

    void OnCollisionExit2D(Collision2D col)
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        if(broken == false)
        {
            
            transform.position = new Vector3(initialpos.x,initialpos.y,0);
        }
    }
}
