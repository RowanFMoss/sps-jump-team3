﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    private Transform playerTransform;
    void Start()
    {
        playerTransform = GameObject.FindObjectOfType<Jumper>().transform;
    }

    
    void Update()
    {
        Vector3 temp = transform.position;
        temp.x = playerTransform.position.x + 5;
        temp.y = playerTransform.position.y + 3;
        transform.position = temp;
        
       
    }
}
