using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.SceneManagement;

public class BatScript : MonoBehaviour
{
    public float batSpeed = 0.5f;
    public float aggroDist;
    public Sprite batSleep;
    public Sprite batMove;
    private bool aggro = false;
    private Transform playerTransform;
    private Transform bearTransform;
    private float bearDistance;
    Vector2 movement;
    [SerializeField] private AudioSource batkill;
    [SerializeField] private AudioSource batsqeak;
    [SerializeField] private AudioClip ACbatsqeak;
    public bool soundplayed;
    // Start is called before the first frame update
    void Start()
    {
        batkill = GetComponent<AudioSource>();
        batsqeak = GetComponent<AudioSource>();
        soundplayed = false;
        GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
    }

    // Update is called once per frame
    void Update()
    {
        
        playerTransform = GameObject.Find("MainPlayer").transform;
        GetComponent<Animator>().enabled = false;
        float playerDistance = Mathf.Sqrt((Mathf.Pow(playerTransform.position.x - this.transform.position.x, 2)) + (Mathf.Pow(playerTransform.position.y - this.transform.position.y, 2)));
        
        Vector3 moveDir = (playerTransform.position - this.transform.position).normalized;
       
        movement.x = moveDir.x * batSpeed * Time.deltaTime;
        movement.y = moveDir.y * batSpeed * Time.deltaTime;
        
        if (aggro == true)
        {
            transform.Translate(movement);
            GetComponent<Animator>().enabled = true;
            if (!soundplayed)
            {
                if (!batsqeak.isPlaying)
                {
                    batsqeak.PlayOneShot(ACbatsqeak);
                    soundplayed = true;
                }
            }
        }
        if ( playerDistance < aggroDist)
        {
            aggro = true;       
        }
        if (GameObject.Find("MainPlayer").tag == "bear" && aggro == true)
        {
            transform.Translate(-(movement * 2));
            batsqeak.Stop();
        }
        if (moveDir.x < 0)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

    }
    void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.name == "MainPlayer")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            batkill.Play();
        }
        
    }
    void OnTriggerEnter2D(Collider2D col)
    {         
        if (col.gameObject.tag == "BatCollider")
        {
            aggro = false;
            //transform.Translate(movement);
            batSpeed = 0.0f;
        }
    }
}