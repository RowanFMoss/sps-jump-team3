﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class VeloDest : MonoBehaviour
{
    public bool broken = false;
    Vector2 initialpos;
    Rigidbody2D dest_rigidbody;
    Vector2 breakpoint;
    Vector2 launchpath;
    public float breakspeed = 6f;
    [SerializeField] private AudioSource bearhitwall;

    [SerializeField] private AudioClip ACbearhitwall;


    // Start is called before the first frame update
    void Start()
    {
        dest_rigidbody = GetComponent<Rigidbody2D>();
        initialpos = this.transform.position;
        dest_rigidbody.constraints = RigidbodyConstraints2D.FreezePosition;
        bearhitwall = GetComponent<AudioSource>();

    }
    void OnCollisionEnter2D(Collision2D col)
    {
        Rigidbody2D rb = col.gameObject.GetComponent<Rigidbody2D>();
        breakpoint = rb.velocity;
        if ((col.gameObject.tag == "bear") && (rb.velocity.magnitude > breakspeed))
        {
            broken = true;
            dest_rigidbody.constraints = RigidbodyConstraints2D.None;
            launchpath = (col.gameObject.transform.position - this.transform.position).normalized;
            GetComponent<Rigidbody2D>().gravityScale = 1;
            GetComponent<Rigidbody2D>().AddForce(-launchpath * 6, ForceMode2D.Impulse);

            if (!bearhitwall.isPlaying)
            {
                bearhitwall.PlayOneShot(ACbearhitwall);
            }

            GetComponent<BoxCollider2D>().enabled = false;
            
        }

    }

    void OnCollisionExit2D(Collision2D col)
    {

    }
    // Update is called once per frame
    void Update()
    {
        if (broken == false)
        {

            transform.position = new Vector3(initialpos.x, initialpos.y, 0);
        }
    }
}
