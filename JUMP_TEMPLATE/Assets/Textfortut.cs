﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Textfortut : MonoBehaviour
{
    public Text Referencetotext;
    public string text;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Referencetotext.text = text;
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        Referencetotext.text = "";
    }


}
